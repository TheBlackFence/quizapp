import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:quizapp/pages/quizResultPage.dart';

class QuizPage extends StatefulWidget {
  final int? initialQuestionIndex;

  QuizPage({this.initialQuestionIndex});

  @override
  _QuizPageState createState() => _QuizPageState(initialQuestionIndex ?? 0);
}


class _QuizPageState extends State<QuizPage> {
  final Map<String, List<Map<String, dynamic>>> questions = {
    'Was ist die Hauptstadt von Deutschland?': [      {'answer': 'Berlin', 'isCorrect': true}, {'answer': 'Berlin', 'isCorrect': true},      {'answer': 'München', 'isCorrect': false},      {'answer': 'Hamburg', 'isCorrect': false},      {'answer': 'Frankfurt', 'isCorrect': false}    ],
    'Wer hat das erste Telefon erfunden?': [      {'answer': 'Alexander Graham Bell', 'isCorrect': true},      {'answer': 'Thomas Edison', 'isCorrect': false},      {'answer': 'Nikola Tesla', 'isCorrect': false},      {'answer': 'Guglielmo Marconi', 'isCorrect': false}    ],
    'Was ist die höchste Bergspitze in der Welt?': [      {'answer': 'Mount Everest', 'isCorrect': true},      {'answer': 'K2', 'isCorrect': false},      {'answer': 'Kangchenjunga', 'isCorrect': false},      {'answer': 'Lhotse', 'isCorrect': false}    ],
  };

  int _currentQuestionIndex = 0;
  int _correctAnswersCount = 0;

  _QuizPageState(this._currentQuestionIndex)
      : _correctAnswersCount = 0;

  void _onAnswerSelected(bool isCorrect) {
    setState(() {
      if (isCorrect) {
        _correctAnswersCount++;
      }
      if (_currentQuestionIndex < questions.length - 1) {
        _currentQuestionIndex++;
      } else {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => QuizResultPage(
              totalQuestionsCount: questions.length,
              correctAnswersCount: _correctAnswersCount,
            ),
          ),
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Quiz'),
      ),
      body: Center(
        child: _currentQuestionIndex < questions.length
            ? Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              questions.keys.toList()[_currentQuestionIndex],
              style: TextStyle(fontSize: 20),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: SizedBox(height: 20),
            ),
            ...questions.values
                .toList()[_currentQuestionIndex]
                .map((answer) => Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: ElevatedButton(
              onPressed: () =>
                    _onAnswerSelected(answer['isCorrect']),
              child: Text(answer['answer']),
              style: ElevatedButton.styleFrom(
                   minimumSize: Size(300, 50),
              )
            ),
                ))
                .toList(),
            SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.all(30.0),
              child: LinearProgressIndicator(
                value: _correctAnswersCount / questions.length,
                minHeight: 30,
                backgroundColor: Colors.grey[200],

              ),
            ),
            SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
                if (_currentQuestionIndex < questions.length - 1) {
                  setState(() {
                    _currentQuestionIndex++;
                  });
                } else {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => QuizResultPage(
                        totalQuestionsCount: questions.length,
                        correctAnswersCount: _correctAnswersCount,
                      ),
                    ),
                  );
                }
              },
              child: Text('Nächste Frage'),
            ),
          ],

        )
            : Text(
          'Sie haben alle Fragen beantwortet! Richtig beantwortet: $_correctAnswersCount/${questions.length}',
          style: TextStyle(fontSize: 20),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}






