import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:quizapp/pages/quizpage.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Quiz App'),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => QuizPage()),
            );
          },
          child: Text('Quiz starten'),
        ),
      ),
    );
  }
}