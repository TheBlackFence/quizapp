import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:quizapp/pages/quizpage.dart';

class QuizResultPage extends StatelessWidget {
  final int totalQuestionsCount;
  final int correctAnswersCount;

  QuizResultPage({
    required this.totalQuestionsCount,
    required this.correctAnswersCount,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Quiz Ergebnis'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Du hast $correctAnswersCount von $totalQuestionsCount Fragen richtig beantwortet.',
              style: TextStyle(fontSize: 20),
            ),
            Padding(
              padding: const EdgeInsets.all(30.0),
              child: LinearProgressIndicator(
                value: correctAnswersCount / totalQuestionsCount,
                minHeight: 30,
                backgroundColor: Colors.grey[200],
              ),
            ),
            SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => QuizPage(),
                  ),
                ); // Navigates back to the quiz page
              },
              child: Text('Quiz neu starten'),
            ),
          ],
        ),
      ),
    );
  }
}